const modalWindow = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");
const closeModalBtn = document.querySelector(".close-modal");
const showModalBtns = document.querySelectorAll(".show-modal");

const closeModal = function () {
   overlay.classList.add("hidden");
   modalWindow.classList.add("hidden");
};

const openModal = function () {
   modalWindow.classList.remove("hidden");
   overlay.classList.remove("hidden");
};

for (let i = 0; i < showModalBtns.length; i++) {
   showModalBtns[i].addEventListener("click", openModal);
}

closeModalBtn.addEventListener("click", closeModal);
overlay.addEventListener("click", closeModal);

document.addEventListener("keydown", function (event) {
   if (event.key === "Escape" && !modalWindow.classList.contains("hidden")) {
      closeModal();
   }
});
