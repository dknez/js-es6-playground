// Simple example to see how the interaction of different functions with the same objects can cause some issues

const flight = "LH234";
const domagoj = {
   name: "Domagoj Knez",
   passport: 248946518,
};

const checkIn = function (flightNum, passenger) {
   flightNum = "AB123";
   passenger.name = "Mr. " + passenger.name;

   if (passenger.passport === 248946518) {
      alert("Check in");
   } else {
      alert("Wrong passport!");
   }
};

checkIn(flight, domagoj);
console.log(flight);
console.log(domagoj);

const newPassport = function (person) {
   person.passport = Math.trunc(Math.random() * 100000000);
};

newPassport(domagoj);
console.log(domagoj);
checkIn(flight, domagoj);

//////////////////////////

const oneWord = function (str) {
   return str.replace(/ /g, "").toLowerCase();
};

const upperFirstWord = function (str) {
   const [first, ...others] = str.split(" ");
   console.log(`First: ${first}`);
   console.log(`Others: ${others}`);
   return [first.toUpperCase(), ...others].join(" ");
};

// higher-order function
const transformer = function (str, fn) {
   console.log(`Original string: ${str}`);
   console.log(`Transformed string: ${fn(str)}`);

   console.log(`Transformed by: ${fn.name}`);
};

transformer("JavaScript is not the best!", upperFirstWord);

transformer("JavaScript is not the best!", oneWord);

const high5 = function () {
   console.log("✋");
};
document.body.addEventListener("click", high5);

["Pero", "Ivo", "Miljenko"].forEach(high5);

///////////////

const greet = function (greeting) {
   return function (name) {
      console.log(`${greeting} ${name}`);
   };
};

const greeterHey = greet("Hey");
greeterHey("Milivoj");
//or
greet("Hello")("Milivoj");

// rewrite using arrow functions

const greet1 = greeting => name => console.log(`${greeting} ${name}`);

greet1("Bok")("Pero");
