pollButton = document.querySelector(".poll");

const poll = {
   question: "What is your favourite programming language?",
   options: ["0: JavaScript", "1: Python", "2: Rust", "3: C++"],
   answers: new Array(4).fill(0),

   registerNewAnswer() {
      const answer = Number(
         prompt(
            `${this.question}\n${this.options.join(
               "\n"
            )}\n(Write option number)`
         )
      );
      console.log(`Selected answer: ${answer}`);

      if (typeof answer === "number" && answer < this.answers.length) {
         this.answers[answer]++;
      }

      this.displayResults("array");
      this.displayResults("string");
      this.displayResults();
   },

   displayResults(type = "array") {
      if (type === "string") {
         console.log(`Poll results are: ${this.answers.join(", ")}`);
      } else if (type === "array") {
         console.log(this.answers);
      }
   },
};

document
   .querySelector(".poll")
   .addEventListener("click", poll.registerNewAnswer.bind(poll));
