// Example 0
const secureBooking = function () {
   let passengerCount = 0;

   return function () {
      passengerCount++;
      console.log(`${passengerCount} passengers`);
   };
};

const booker = secureBooking();

booker();
booker();
booker();

// Example 1
let f;

const g = function () {
   const a = 23;
   f = function () {
      console.log(a * 2);
   };
};

const h = function () {
   const b = 33;
   f = function () {
      console.log(b * 2);
   };
};

g();
f();
console.dir(f);

// Re-assigning f function
h();
f();
console.dir(f);

// Example 2 - timer
// const boardPassengers = function (numberOfPassengers, wait) {
//    // divide the passenger in three groupds
//    const perGroup = numberOfPassengers / 3;

//    // callback function
//    setTimeout(function () {
//       console.log(`We are now boardling all ${n} passengers`);
//       console.log(`There are 3 groups, each with ${perGroup} passengers`);
//    }, wait * 1000);

//    console.log(`Will start boarding in ${wait} seconds`);
// };

// boardPassengers(180, 3);

const blje = function () {
   const header = document.querySelector("h1");
   header.style.color = "red";

   document.body.addEventListener("click", function () {
      header.style.color = "blue";
   });
};

blje();
console.dir(blje);
