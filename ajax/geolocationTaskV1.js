const btn = document.querySelector(".btn-country");
const countriesContainer = document.querySelector(".countries");

const reverseGeocodingApi = "https://geocode.xyz/";

btn.style.visibility = "hidden";

const renderCountry = function (data, className = "") {
   console.log(data.name);
   const html = `
     <article class="country ${className}">
     <img class="country__img" src="${data.flag}" />
     <div class="country__data">
       <h3 class="country__name">${data.name}</h3>
       <h4 class="country__region">${data.region}</h4>
       <p class="country__row"><span>👫</span>${(
          +data.population / 1000000
       ).toFixed(1)} mil. people</p>
       <p class="country__row"><span>🗣️</span>${data.languages[0].name}</p>
       <p class="country__row"><span>💰</span>${data.currencies[0].name}</p>
     </div>
   </article>
  `;

   countriesContainer.insertAdjacentHTML("beforeend", html);
   countriesContainer.style.opacity = 1;
};

const whereAmI = function (lat, lng) {
   fetch(`${reverseGeocodingApi}${lat},${lng}?geoit=json`)
      .then(response => {
         if (!response.ok)
            throw new Error(`Problem with geocoding: ${response.status}`);
         return response.json();
      })
      .then(data => {
         console.log(`You are in ${data.city}, ${data.country}`);
         return fetch(
            `https://restcountries.eu/rest/v2/name/${data.country}?fullText=true`
         );
      })
      .then(response => response.json())
      .then(data => {
         renderCountry(data[0]);
      })
      .catch(error => console.log(`Something bad happened: ${error.message}`));
};

whereAmI(52.508, 13.381);
whereAmI(19.037, 72.873);
whereAmI(-33.933, 18.474);
whereAmI(39.930772398777066, 116.43108756780813);
