// building a promise
// simulate a lottery: fulfilled promise = win a lottery

const lotteryPromise = new Promise(function (resolve, reject) {
   console.log("Lottery draw is happening 🤑");

   setTimeout(function () {
      if (Math.random() >= 0.5) {
         resolve("You won! 💰");
      } else {
         reject(new Error("You lost! 💩"));
      }
   }, 2000);
});

// consume the promise that we just built
lotteryPromise
   .then(result => console.log(result))
   .catch(error => console.error(error));

// promisify setTimeout function
const wait = function (seconds) {
   return new Promise(function (resolve) {
      setTimeout(resolve, seconds * 1000);
   });
};

const myPromise = wait(2);
myPromise
   .then(() => {
      console.log("I waiter for 2 seconds");
      return wait(1);
   })
   .then(() => console.log("I waited for 1 second"));
