// promisifying the geolocation API
const btn = document.querySelector(".btn-country");
const countriesContainer = document.querySelector(".countries");
const reverseGeocodingApi = "https://geocode.xyz/";

const renderCountry = function (data, className = "") {
   console.log(data.name);
   const html = `
      <article class="country ${className}">
      <img class="country__img" src="${data.flag}" />
      <div class="country__data">
        <h3 class="country__name">${data.name}</h3>
        <h4 class="country__region">${data.region}</h4>
        <p class="country__row"><span>👫</span>${(
           +data.population / 1000000
        ).toFixed(1)} mil. people</p>
        <p class="country__row"><span>🗣️</span>${data.languages[0].name}</p>
        <p class="country__row"><span>💰</span>${data.currencies[0].name}</p>
      </div>
    </article>
   `;

   countriesContainer.insertAdjacentHTML("beforeend", html);
   countriesContainer.style.opacity = 1;
};

const getJson = function (url, errorMessage = "Something went wrong") {
   return fetch(url).then(response => {
      if (!response.ok) throw new Error(`${errorMessage} ${response.status}`);

      return response.json();
   });
};

const getPosition = function () {
   return new Promise(function (resolve, reject) {
      //   navigator.geolocation.getCurrentPosition(
      //      position => resolve(position),
      //      err => reject(err)
      //   );
      navigator.geolocation.getCurrentPosition(resolve, reject);
   });
};

const whereAmI = function () {
   getPosition()
      .then(position => {
         const { latitude: lat, longitude: lng } = position.coords;
         console.log(`Latitude: ${lat}, Longitude: ${lng}`);
         return fetch(`${reverseGeocodingApi}${lat},${lng}?geoit=json`);
      })
      .then(response => {
         console.log(response);
         if (!response.ok)
            throw new Error(`Problem with geocoding: ${response.status}`);
         return response.json();
      })
      .then(data => {
         console.log(`You are in ${data.city}, ${data.country}`);
         return fetch(
            `https://restcountries.eu/rest/v2/name/${data.country}?fullText=true`
         );
      })
      .then(response => response.json())
      .then(data => {
         renderCountry(data[0]);
      })
      .catch(error => console.log(`Something bad happened: ${error.message}`));
};

btn.addEventListener("click", whereAmI);
