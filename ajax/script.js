const btn = document.querySelector(".btn-country");
const countriesContainer = document.querySelector(".countries");
const imgContainer = document.querySelector(".images");

// const renderCountry = function (data, className = "") {
//    console.log(data.name);
//    const html = `
//       <article class="country ${className}">
//       <img class="country__img" src="${data.flag}" />
//       <div class="country__data">
//         <h3 class="country__name">${data.name}</h3>
//         <h4 class="country__region">${data.region}</h4>
//         <p class="country__row"><span>👫</span>${(
//            +data.population / 1000000
//         ).toFixed(1)} mil. people</p>
//         <p class="country__row"><span>🗣️</span>${data.languages[0].name}</p>
//         <p class="country__row"><span>💰</span>${data.currencies[0].name}</p>
//       </div>
//     </article>
//    `;

//    countriesContainer.insertAdjacentHTML("beforeend", html);
//    countriesContainer.style.opacity = 1;
// };

// const getJson = async function (url, errorMessage = "Something went wrong") {
//    const response = await fetch(url);
//    if (!response.ok) throw new Error(`${errorMessage} ${response.status}`);
//    return await response.json();
// };

// const getPosition = function () {
//    return new Promise(function (resolve, reject) {
//       //   navigator.geolocation.getCurrentPosition(
//       //      position => resolve(position),
//       //      err => reject(err)
//       //   );
//       navigator.geolocation.getCurrentPosition(resolve, reject);
//    });
// };

// const whereAmI = async function (country) {
//    try {
//       // geolocation
//       const position = await getPosition();
//       const { latitude: lat, longitude: lng } = position.coords;

//       // reverse geocoding
//       const geoResponse = await fetch(
//          `https://geocode.xyz/${lat},${lng}?geoit=json`
//       );
//       if (!geoResponse.ok) throw new Error("Problem getting location data");
//       const geoData = await geoResponse.json();

//       const response = await fetch(
//          `https://restcountries.eu/rest/v2/name/${geoData.country}`
//       );
//       if (!response.ok) throw new Error("Problem getting country");

//       const data = await response.json();
//       renderCountry(data[0]);

//       return `You are in ${geoData.city}, ${geoData.country}`;
//    } catch (err) {
//       console.log(err);

//       throw err;
//    }
// };

// console.log("1: Will get location");
// this will only log promise
// const city = whereAmI();
// console.log(city);
// so we should do it like this:
// whereAmI().then(result => console.log(result));
// console.log("3: Finished getting location");

// (async function () {
//    try {
//       const myLocation = await whereAmI();
//       console.log(`2: ${myLocation}`);
//    } catch (err) {
//       console.log(`2: ${err.message}`);
//    }
//    console.log("3: Finished getting location");
// })();

// const get3Countries = async function (country1, country2, country3) {
//    try {
//       //   const [data1] = await getJson(
//       //      `https://restcountries.eu/rest/v2/name/${country1}`
//       //   );
//       //   const [data2] = await getJson(
//       //      `https://restcountries.eu/rest/v2/name/${country2}`
//       //   );
//       //   const [data3] = await getJson(
//       //      `https://restcountries.eu/rest/v2/name/${country3}`
//       //   );
//       //   console.log([data1.capital, data2.capital, data3.capital]);

//       const data = await Promise.all([
//          getJson(`https://restcountries.eu/rest/v2/name/${country1}`),
//          getJson(`https://restcountries.eu/rest/v2/name/${country2}`),
//          getJson(`https://restcountries.eu/rest/v2/name/${country3}`),
//       ]);
//       console.log(data);
//       console.log(data.map(data => data[0].capital));
//    } catch (error) {
//       console.log(error);
//    }
// };

// get3Countries("belarus", "canada", "tanzania");

// // Create a custom timeout promise
// const timeout = function (sec) {
//    return new Promise(function (_, reject) {
//       setTimeout(function () {
//          reject(new Error("Reject took too long!"));
//       }, sec * 1000);
//    });
// };

// // Promise.race
// (async function () {
//    const [res] = await Promise.race([
//       getJson(`https://restcountries.eu/rest/v2/name/italy`),
//       getJson(`https://restcountries.eu/rest/v2/name/egypt`),
//       getJson(`https://restcountries.eu/rest/v2/name/mexico`),
//    ]);
//    console.log(res);
// })();

// Promise.race([
//    getJson(`https://restcountries.eu/rest/v2/name/tanzania`),
//    timeout(1),
// ])
//    .then(response => console.log(response[0]))
//    .catch(err => console.error(err));

// // Promise.allSettled
// Promise.allSettled([
//    Promise.resolve("Success"),
//    Promise.reject("ERROR"),
//    Promise.resolve("Another success"),
// ]).then(response => console.log(response));

// // Promise.any
// Promise.any([
//    Promise.resolve("Success"),
//    Promise.reject("ERROR"),
//    Promise.resolve("Another success"),
// ])
//    .then(response => console.log(response))
//    .catch(error => console.log(error));

// Promise.any([Promise.reject("ERROR"), Promise.reject("ERROR 2")])
//    .then(response => console.log(response))
//    .catch(error => console.log(error));

// Coding challenge 3
const wait = function (seconds) {
   return new Promise(function (resolve) {
      setTimeout(resolve, seconds * 1000);
   });
};

const createImage = async function (imgPath) {
   return new Promise(function (resolve, reject) {
      console.log("Getting image");

      let img = document.createElement("img");
      img.src = imgPath;

      img.addEventListener("load", function () {
         imgContainer.append(img);
         resolve(img);
      });

      img.addEventListener("error", function () {
         reject(new Error("Image not found. ❌"));
      });
   });
};

// const loadNPause = async function () {
//    try {
//       let img = await createImage("img/img-1.jpg");
//       console.log("Image 1 loaded");
//       await wait(2);
//       img.style.display = "none";

//       img = await createImage("img/img-2.jpg");
//       console.log("Image 2 loaded");
//       await wait(2);
//       img.style.display = "none";

//       img = await createImage("img/img-3.jpg");
//       console.log("Image 3 loaded");
//       await wait(2);
//       img.style.display = "none";
//    } catch (error) {
//       console.log(`I got this error: ${error} 💥`);
//    }
// };

// loadNPause(images);

const images = ["img/img-1.jpg", "img/img-2.jpg", "img/img-3.jpg"];

const loadAll = async function (imgArr) {
   try {
      const imgs = imgArr.map(image => createImage(image));
      console.log(imgs);

      const imgsEl = await Promise.allSettled(imgs);
      console.log(imgsEl);

      imgsEl.array.forEach(element => img.classList.add("parallel)"));
   } catch (error) {
      console.log("UPS: " + error);
   }
};

loadAll(images);
