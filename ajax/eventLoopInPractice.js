// microtasks and callback queue
console.log("Test start");

setTimeout(() => console.log("0 sec timer"), 0);

Promise.resolve("Resolved promise 1").then(response => console.log(response));

Promise.resolve("Resolved promise 2").then(response => {
   for (let i = 0; i < 100000; i++) {
      // simulation: this "smart job" takes long to be resolved}
      console.log(response);
   }
});

console.log("Test end");
