const maxGeneratedNumber = 20;
const maxAttempts = 20;
const checkButton = document.querySelector(".check");
const againButton = document.querySelector(".again");
const scoreField = document.querySelector(".score");
const messageField = document.querySelector(".message");
const highScoreField = document.querySelector(".highscore");
const numberField = document.querySelector(".number");
const guessField = document.querySelector(".guess");

const noNumberProvided = "⛔️ No number provided!";
const correctNumber = "🎉 Correct number!";
const guessTooHigh = "📈 Too high!";
const guessTooLow = "📉 Too low!";
const gameLost = "🤯 You lost the game!";
const startGuessing = "Start guessing...";

let secretNumber;
let score;
let highScore = 0;

const displayMessage = function (message) {
   messageField.textContent = message;
};

function initializeGame() {
   secretNumber = Math.trunc(Math.random() * maxGeneratedNumber) + 1;
   score = maxAttempts;
   displayMessage(startGuessing);
   guessField.value = "";
   numberField.textContent = "?";
   scoreField.textContent = maxAttempts;
   resetBackgroundColor();
}

function resetBackgroundColor() {
   document.querySelector("body").style.backgroundColor = "#222";
}

function changeBackgroundColorToGreen() {
   document.querySelector("body").style.backgroundColor = "#60b347";
}

function checkAndSetHighScore() {
   if (score > highScore) {
      highScore = score;
   }

   highScoreField.textContent = highScore;
}

initializeGame();

checkButton.addEventListener("click", function () {
   const guess = Number(document.querySelector(".guess").value);
   console.log(guess);
   score--;

   if (!guess) {
      displayMessage(noNumberProvided);
   } else if (guess === secretNumber) {
      scoreField.textContent = score;
      displayMessage(correctNumber);
      numberField.textContent = secretNumber;
      checkAndSetHighScore();
      changeBackgroundColorToGreen();
   } else if (guess !== secretNumber) {
      if (score > 1) {
         guess > secretNumber
            ? displayMessage(guessTooHigh)
            : displayMessage(guessTooLow);
         scoreField.textContent = score;
      } else {
         scoreField.textContent = gameLost;
      }
   }
});

againButton.addEventListener("click", function () {
   initializeGame();
});
