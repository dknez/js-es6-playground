// constructor function
// const Person = function (firstName, birthYear) {
//    this.firstName = firstName;
//    this.birthYear = birthYear;
// };

// const domagoj = new Person("Domagoj", 1987);
// console.log(domagoj);

// // class declaration
// class Person1 {
//    constructor(firstName, birthYear) {
//       this.firstName = firstName;
//       this.birthYear = birthYear;
//    }
// }

// const domagoj1 = new Person1("Domagoj", 1987);
// console.log(domagoj1);

// // Prototypes
// console.log(Person.prototype);

// Person.prototype.calcAge = function () {
//    console.log(2037 - this.birthYear);
// };

// domagoj === Person;

// domagoj.calcAge();
// console.log(domagoj.__proto__);

// console.log(domagoj.__proto__ === Person.prototype);
// console.log(Person.prototype.isPrototypeOf(domagoj));

// Person.prototype.species = "Homo Sapiens";
// console.log(domagoj, domagoj1);

// console.log(domagoj.hasOwnProperty("firstName"));
// console.log(domagoj.hasOwnProperty("species"));

// const arr = [3, 5, 5, 6];
// console.log(arr.__proto__);
// console.log(arr.__proto__ === Array.prototype);

// console.log(arr.__proto__.__proto__);

// // Coding challenge

// class Car {
//    constructor(make, speed) {
//       this.make = make;
//       this.speed = speed;
//    }

//    accelerate() {
//       this.speed += 10;
//       console.log(`${this.make}'s speed is ${this.speed}`);
//    }

//    brake() {
//       this.speed -= 5;
//       console.log(`${this.make}'s speed is ${this.speed}`);
//    }

//    get speedUS() {
//       return this.speed / 1.6;
//    }

//    set speedUS(speed) {
//       this.speed = speed * 1.6;
//    }
// }

// const car1 = new Car("Audi", 150);
// const car2 = new Car("VW", 120);

// car1.accelerate();
// car1.accelerate();

// car2.brake();
// car2.brake();

// // Object.create examples
// const PersonProto = {
//    calcAge() {
//       console.log(2037 - this.birthYear);
//    },
// };

// const steven = Object.create(PersonProto);

// console.log(car1.speedUS);
// console.log(car1.speed);

// console.log(car2.speedUS);
// console.log(car2.speed);

// car1.speedUS = 50;
// console.log(car1.speed);
// console.log(car2.speedUS);

// inheritance between classes

// const Person = function (firstName, birthYear) {
//    this.firstName = firstName;
//    this.birthYear = birthYear;
// };

// Person.prototype.calcAge = function () {
//    console.log(2037 - this.birthYear);
// };

// const Student = function (firstName, birthYear, course) {
//    Person.call(this, firstName, birthYear);
//    this.course = course;
// };

// // Linking prototypes
// Student.prototype = Object.create(Person.prototype);

// Student.prototype.introduce = function () {
//    console.log(`My name is ${this.firstName} and I study ${this.course}`);
// };

// const mike = new Student("Mike", 2020, "Computer Science");
// console.log(mike);
// mike.introduce();
// mike.calcAge();

// inheritance between classes in ES6

class Person {
   constructor(firstName, birthYear) {
      this.firstName = firstName;
      this.birthYear = birthYear;
   }

   calcAge() {
      console.log(2037 - this.birthYear);
   }

   getAge() {
      return 2037 - this.birthYear;
   }
}

class Student extends Person {
   constructor(firstName, birthYear, course) {
      super(firstName, birthYear);
      this.course = course;
   }

   introduce() {
      console.log(`My name is ${this.firstName} and I study ${this.course}`);
   }

   calcAge() {
      console.log(
         `I'm ${
            2037 - this.birthYear
         } years old but as a student I feel more like ${
            2037 - this.birthYear + 10
         }`
      );
   }
}

const martha = new Student("Martha", 2012, "Computer Science");
martha.introduce();
martha.calcAge();
