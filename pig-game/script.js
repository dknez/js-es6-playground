// const score1 = document.querySelector("#score--0");
// const score2 = document.querySelector("#score--1");

const maxGeneratedNumber = 6;
const scoreToWin = 100;

const player1Element = document.querySelector(".player--0");
const player2Element = document.querySelector(".player--1");
const player1ScoreElement = document.getElementById("score--0");
const player2ScoreElement = document.getElementById("score--1");
const newGameButton = document.querySelector(".btn--new");
const rollButton = document.querySelector(".btn--roll");
const holdButton = document.querySelector(".btn--hold");
const diceElement = document.querySelector(".dice");
const player1CurrentScoreElement = document.getElementById("current--0");
const player2CurrentScoreElement = document.getElementById("current--1");

let scores, currentScore, activePlayer;

const init = function () {
   console.log("Game initalized");
   player1ScoreElement.textContent = 0;
   player2ScoreElement.textContent = 0;
   player1CurrentScoreElement.textContent = 0;
   player2CurrentScoreElement.textContent = 0;
   player1Element.classList.remove("player--winner");
   player2Element.classList.remove("player--winner");
   diceElement.classList.add("hidden");
   player1Element.classList.add("player--active");
   scores = [0, 0];
   currentScore = 0;
   activePlayer = 0;
   enableButtons();
};

// const switchPlayer = function () {
//    ...
// }

function switchPlayer() {
   document.getElementById(`current--${activePlayer}`).textContent = 0;
   currentScore = 0;
   activePlayer = activePlayer === 0 ? 1 : 0;
   player1Element.classList.toggle("player--active");
   player2Element.classList.toggle("player--active");
}

function updateCurrentScore() {
   document.getElementById(
      `current--${activePlayer}`
   ).textContent = currentScore;
}

function updateTotalScore() {
   scores[activePlayer] += currentScore;
   document.getElementById(`score--${activePlayer}`).textContent =
      scores[activePlayer];
}

function declareWinner() {
   document
      .querySelector(`.player--${activePlayer}`)
      .classList.remove("player--active");
   document
      .querySelector(`.player--${activePlayer}`)
      .classList.add("player--winner");
}

function disableButtons() {
   rollButton.disabled = true;
   holdButton.disabled = true;
}

function enableButtons() {
   rollButton.disabled = false;
   holdButton.disabled = false;
}

init();

rollButton.addEventListener("click", function () {
   const diceRoll = Math.trunc(Math.random() * maxGeneratedNumber) + 1;

   diceElement.classList.remove("hidden");
   diceElement.src = `dice-${diceRoll}.png`;

   if (diceRoll != 1) {
      currentScore += diceRoll;
      updateCurrentScore();
   } else {
      switchPlayer();
   }
});

holdButton.addEventListener("click", function () {
   console.log("Hold button clicked.");
   updateTotalScore();

   if (scores[activePlayer] >= scoreToWin) {
      declareWinner();
      disableButtons();
   } else {
      switchPlayer();
   }
});

newGameButton.addEventListener("click", init);
